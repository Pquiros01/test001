import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ShorterUrl } from '../lib/persistence/shorterurl.js';

import './main.html';
import './home.html';
import './shortenerUrlSection.html';

/*
Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});*/

Template.home.events({
  'submit .shorter-url-form'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    // Get value from form element
    const target = event.target;
    const text = target.text.value;
    ShorterUrl.insert({
      url: text,
      shorter_url: text+"000123",
      counter:0
    });
    // Clear form
    target.text.value = '';
    Router.go("/view/"+text+"000123"); 
  },
});



