import { ShorterUrl } from '../persistence/shorterurl.js';

Router.route('/', {
    template: 'home'
});

Router.route('/:_new_short_url', {where: 'server'}).get(function() {
  this.response.writeHead(302, {
    'Location': "https://www.google.com"
  });
  this.response.end();
});

Router.route('/view/:_short_url', {
	name:"shortenerUrl",
    template: 'shortenerUrlSection',
    data: function () {
    	 console.log(ShorterUrl.find({}));
    	//console.log(ShorterUrl.find({shorter_url: "rew000123"}, {fields: {'url':1}}));
    	return {_short_url: Meteor.absoluteUrl() + this.params._short_url};
	}
});